## Client install

```
git clone https://lga-dev@bitbucket.org/lga-dev/alkemy-react-challenge.git

cd alkemy-react-challenge

npm install

npm start
```

## Client url

```
http://localhost:3000/
```

## Server install
```
npx json-server --watch src/data/db.json --port 8000
```

## Server url

```
http://localhost:8000/blogs
```