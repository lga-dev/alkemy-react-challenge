import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Navbar from './Navbar'
import Home from './Home'
import BlogCreate from './BlogCreate'
import BlogDetails from './BlogDetails'
import BlogUpdate from './BlogUpdate'

function App() {
  return (
    <Router>
      <div className="App">
        <Navbar />
        <div className="wrapper">
          <Switch>
            <Route exact path="/">
              <Home />
            </Route>
            <Route path="/create">
              <BlogCreate />
            </Route>
            <Route path="/blogs/:id">
              <BlogDetails />
            </Route>
            <Route path="/update/:id">
              <BlogUpdate />
            </Route>
          </Switch>
        </div>
      </div>
    </Router>
  );
}

export default App;
