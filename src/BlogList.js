import { Link } from "react-router-dom";

const BlogList = ({blogs, title}) => {
  const handleDelete = (blog) => {
    console.log("Borrando post...")
    fetch('http://localhost:8000/blogs/' + blog.id, {
      method: 'DELETE'
    })
  }	

  return (
    <div className="blog-list">
      <h2 className="section-title">{ title }</h2>
      {blogs.map((blog) => (
        <div className="blog-preview" key={ blog.id }>
          <p className="blog-title" >{ blog.title }</p>
          <div className="blog-button-group">
            <Link to={`/blogs/${ blog.id }`}>
              <button className="blog-button">Detalles</button>
            </Link>
            <Link to={`/update/${ blog.id }`}>
              <button className="blog-button">Actualizar</button>
            </Link>
            <button onClick={ () => handleDelete(blog) } className="blog-button">Borrar</button>
          </div>
        </div>
      ))}
    </div>
  );
}

export default BlogList;