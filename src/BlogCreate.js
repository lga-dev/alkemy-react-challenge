import { useState } from "react";

const BlogCreate = () => {
  const [title, setTitle] = useState('');
  const [body, setBody] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  const handleSubmit = (e) => {
    e.preventDefault();
    const blog = { title, body };
    setIsLoading(true);

    fetch('http://localhost:8000/blogs' , {
      method: 'POST',
      headers: {"Content-Type": "application/json"},
      body: JSON.stringify(blog)
    })
    .then(() => {
      setIsLoading(false)
    })
  }

  return (
    <div className="blog-create">
      <h2 className="section-title">Agregar un nuevo post</h2>
      <form onSubmit={ handleSubmit }>
        {/* Title input */}
        <label>Titulo del post:</label>
        <input type="text" required value={ title } onChange={(e) => setTitle(e.target.value)}/>
        {/* Content input */}
        <label>Contenido del post:</label>
        <textarea required value={ body } onChange={(e) => setBody(e.target.value)}/>
        {/* Conditional rendering */}
        {!isLoading && <button className="blog-button">Agregar post</button>}
        {isLoading && <button className="blog-button" disabled>Agregando nuevo post...</button>}
      </form>
    </div>
  );
}

export default BlogCreate;