import { useState, useEffect } from 'react'

const useFetch = (url) => {
  const [data, setData] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(null);
  const abortContr = new AbortController();

  useEffect(() => {
    fetch(url, { signal: abortContr.signal })
      .then(response => {
        if(!response.ok) {
          throw Error('Could not fetch the data for that resource :(')
        }
        return response.json()
      })
      .then(data => {
        setData(data);
        setIsLoading(false);
        setError(null);
      })
      .catch(err => {
        if(err.name === 'AbortError') {
          console.log("Fetch aborted :(")
        } else {
          setIsLoading(false);
          setError(err.message);
        }
      })

      return () => abortContr.abort();
  }, [url]);

  return{ data, isLoading, error}
}

export default useFetch;