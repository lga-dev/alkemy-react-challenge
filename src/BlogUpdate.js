import { useState } from "react";
import { useParams } from "react-router";
import useFetch from './useFetch';

const BlogUpdate = () => {
  const { id } = useParams();
  const { error } = useFetch('http://localhost:8000/blogs/' + id);
  const [title, setTitle] = useState('');
  const [body, setBody] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  const handleSubmit = (e) => {
    e.preventDefault();
    const newBlog = { title, body };
    setIsLoading(true);
    console.log("Actualizando post...")

    fetch('http://localhost:8000/blogs/' + id, {
      method: 'PUT',
      headers: {"Content-Type": "application/json"},
      body: JSON.stringify(newBlog)
    })
    .then(() => {
      setIsLoading(false)
    })
  }

  return (
    <div className="blog-update">
      <h2 className="section-title">Actualizar post</h2>
      <form onSubmit={ handleSubmit }>
        {/* Title input */}
        <label>Titulo del post:</label>
        <input type="text" required value={ title } onChange={(e) => setTitle(e.target.value)}/>
        {/* Content input */}
        <label>Contenido del post:</label>
        <textarea required value={ body } onChange={(e) => setBody(e.target.value)}/>
        {/* Conditional rendering */}
        { error && <div>{ error }</div> }
        { !isLoading && <button className="blog-button">Actualizar post</button> }
        { isLoading && <button className="blog-button" disabled>Agregando post...</button> }
      </form>
    </div>
  );
}

export default BlogUpdate;