import { Link } from "react-router-dom";

const Navbar = () => {
  return (
    <nav className="navbar">
      <h1 className="navbar-title">The Blog</h1>
      <div>
        <Link className="navbar-links" to={"/"}>
          Home
        </Link>
        <Link className="navbar-links" to={"/create"}>
          Crear nuevo post
        </Link>
      </div>
    </nav>
  );
}

export default Navbar;